# Kilavo Federation Registry

This repository is a temporary Federation registry for Kilavo.
This is used for the Proof-of-concept, eventually, this will be a smart contract
on the tezos blockchain, so that everybody can start their own Federation and request
to join existing ones.